﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace UP.Proyecto.func
{
    [Serializable]
    public class Data
    {
        static private BinaryFormatter bf;

        public double[,] Coords { get; set; }

        public void Serialize(Stream S)
        {
            if(bf==null) bf = new BinaryFormatter(); 
            bf.Serialize(S, this);
        }

        public static Data Deserialize(Stream S)
        {
            if (bf == null) bf = new BinaryFormatter();
            return  (Data) bf.Deserialize(S);
        }

    }
}
