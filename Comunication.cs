﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Net;
using System.IO;

namespace UP.Proyecto.func
{
    public class ComManda
    {
        private TcpListener TcpListenerServer { get; set; }
        object Data;
        string IP = null;
        int Port = 0;
        private void IniciarConexion(IAsyncResult piasResultado)
        {
            //// Obtengo el cliente que inició una 
            TcpClient cliente = piasResultado.AsyncState as TcpClient;
            using (NetworkStream clienteStream = cliente.GetStream())
            {

                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(clienteStream, Data);
                clienteStream.Flush();
                clienteStream.Close();
            }
            cliente.Close();

        }

        public ComManda(string IP1, int Port1)
        {
            IP = IP1;
            Port = Port1;

        }

        public void Mandar(object Datos)
        {
            Data = Datos;
            TcpClient clienteTCP = new TcpClient(AddressFamily.InterNetwork);
            IPAddress[] listaHost = Dns.GetHostAddresses(IP);
            clienteTCP.BeginConnect(listaHost, Port, new AsyncCallback(IniciarConexion), clienteTCP);
        }
    }

    public class ComRecibe
    {
       // public object Data { get; set; }
        private TcpListener TcpListenerServer { get; set; }
        public event EventHandler Recibi;
        int Port = 0;
        private void IniAceptacionDeConexiones()
        {
            // Iniciamos un método asincrono para que notifique cuando se acepta una conexion Tcp
            this.TcpListenerServer.BeginAcceptTcpClient(new AsyncCallback(AceptarClienteTCP), TcpListenerServer);
        }
        //cons
        public ComRecibe(int Port1) { Port = Port1; }
        //inicia escuchar
        public void Escuchar()
        { // Iniciamos
            this.TcpListenerServer = new TcpListener(IPAddress.Any, Port); // Con IPAddress.Any le decimos al server que escuche en todas las interfaces
            this.TcpListenerServer.Start(); // Iniciamos el servidor
            IniAceptacionDeConexiones();
        }

        private void AceptarClienteTCP(IAsyncResult piasResultado)
        {
            IniAceptacionDeConexiones();

            // Obtengo el server
            TcpListener listenerTCP = (TcpListener)piasResultado.AsyncState;
            // Obtengo el cliente que inició la conexion 
            TcpClient clienteTCP = listenerTCP.EndAcceptTcpClient(piasResultado);


            using (NetworkStream clientStream = clienteTCP.GetStream())
            {
                try
                {
                    Byte[] byBuffer = new Byte[clienteTCP.ReceiveBufferSize];
                    List<byte> miArchivo = new List<byte>(); // Lo puse como lista por que no sabemos cuanto va a ocupar el archivo
                    int iLength;

                    while ((iLength = clientStream.Read(byBuffer, 0, byBuffer.Length)) != 0)
                    {
                        byte[] byCopia = new byte[iLength];
                        Array.Copy(byBuffer, 0, byCopia, 0, iLength);
                        miArchivo.AddRange(byCopia); // Y lo agregamos a la lista
                    }
                    MemoryStream ms = new MemoryStream(miArchivo.ToArray());
                    IFormatter formatter = new BinaryFormatter();
                    object obj = formatter.Deserialize(ms);
                    clientStream.Close();
                    //...
                    RecibeEventArgs A = new RecibeEventArgs();
                    A.Datos = obj;
                    //this.Data = obj;
                    if (Recibi != null) Recibi(this, A);
                }
                catch (SystemException e)
                {
                    // Error
                }
                finally
                {
                    // Terminamos la conexion
                    clienteTCP.Close();
                }
            }
        }
    }

    public class RecibeEventArgs : EventArgs
    {
        public object Datos { get; set; }
    }
}
